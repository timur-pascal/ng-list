import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'form-list',
  templateUrl: 'form.component.html',
  styleUrls: ['form.component.css']
})
export class Form {
  name = '';
  
  @Output() add = new EventEmitter();

  onSubmit() {
    this.add.emit(this.name);
    this.name = '';
  }
}