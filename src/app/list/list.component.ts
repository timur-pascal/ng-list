import { Component, Input } from '@angular/core';

@Component({
  selector: 'items-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.css']
})

export class List {
  @Input() items; 

  delete(item) {
    console.log(item);
    let index = this.items.indexOf(item);
    this.items.slice(index);
    

    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

}